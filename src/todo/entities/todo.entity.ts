/**
 * Fields to implement
 * id -> number
 * title -> string
 * description -> string
 * completed -> boolean
 * assignee -> User (relation to new entity user)
 */
export class Todo {}
